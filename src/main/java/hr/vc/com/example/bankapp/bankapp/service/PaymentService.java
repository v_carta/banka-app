package hr.vc.com.example.bankapp.bankapp.service;

import hr.vc.com.example.bankapp.bankapp.model.Payment;

import java.util.List;

public interface PaymentService {
    Payment createPayment(Payment payment);
    Payment getPayment(Long id);
    Payment editPayment(Payment payment);
    List<Payment> getAllPayments();
    List<Payment> findByStatus(String status);
    List<Payment> getMyPayments(List<Payment> allPayments, Long id);

}