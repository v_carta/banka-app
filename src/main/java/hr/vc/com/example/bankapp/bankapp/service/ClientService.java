package hr.vc.com.example.bankapp.bankapp.service;

import hr.vc.com.example.bankapp.bankapp.model.Client;

import java.util.List;
public interface ClientService {
    Client createClient(Client client);
    Client getClient(Long id);
    Client editClient(Client person);
    List<Client> getAllClients();
    Client getClientByUsername(String username);
    List<Client> getOtherClients(List<Client> allClients, Long id);
}
