package hr.vc.com.example.bankapp.bankapp.service;

import hr.vc.com.example.bankapp.bankapp.model.Client;
import hr.vc.com.example.bankapp.bankapp.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client createClient(Client client) {
        Long randomNumber;
        Client findClient;
        do {
            long range = 9999999999l - (long) 0 + 1;
            Random rand = new Random();
            long fraction = (long) (range * rand.nextDouble());
            randomNumber = fraction + (long) 0;
            findClient = clientRepository.findByAccount(randomNumber);
        }while( findClient != null);
        client.setAccount(randomNumber);
        return clientRepository.save(client);
    }

    @Override
    public Client getClient(Long id) {
        return clientRepository.findOne(id);
    }

    @Override
    public Client editClient(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public Client getClientByUsername(String username) {
        return clientRepository.findByUsername(username);
    }

    @Override
    public List<Client> getOtherClients(List<Client> allClients, Long id) {
        List<Client> otherClients = new LinkedList<>();
        for (Client allClient : allClients) {
            if ((allClient.getId() != id) && allClient.getUsername() != "bank" ) {
                otherClients.add(allClient);
            }
        }
        return otherClients;
    }
}