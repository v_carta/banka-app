package hr.vc.com.example.bankapp.bankapp;

import hr.vc.com.example.bankapp.bankapp.service.PaymentService;
import hr.vc.com.example.bankapp.bankapp.model.Client;
import hr.vc.com.example.bankapp.bankapp.service.ClientService;
import hr.vc.com.example.bankapp.bankapp.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;

@Controller
@SessionAttributes("user")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private PaymentService paymentService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String homePage(Model model, @RequestParam(value = "err", required = false) String err) {
        if (err != null && err.length() > 0) {
            model.addAttribute("error", "Wrong username or password.");
        } else {
            model.addAttribute("error", "");
        }
        model.addAttribute("client", new Client());
        return "login";
    }

    @RequestMapping(value = "/makeAccount", method = RequestMethod.POST)
    public String check(@ModelAttribute Client client, Model model) {
        String error = "error";
        Client currentClient = clientService.getClientByUsername(client.getUsername());

        if (currentClient != null && currentClient.getUsername().equals("bank") && currentClient.getPassword().equals(client.getPassword())) {
            model.addAttribute("user", currentClient);
            return "redirect:/bank?status=all";
        } else if (currentClient == null && client.getUsername().length() > 0 && client.getPassword().length() > 3) {
            currentClient = clientService.createClient(client);
            model.addAttribute("user", currentClient);
            return "redirect:/account?status=all";
        } else if (currentClient == null || !currentClient.getPassword().equals(client.getPassword())) {
            model.addAttribute("error", error);
            return "redirect:/?err=1";
        } else {
            model.addAttribute("user", currentClient);
            return "redirect:/account?status=all";
        }
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public String accountView(@RequestParam(value = "status") String status, @RequestParam(value = "err", required = false) String err, Model model, HttpServletRequest request) {
        if (err != null && err.length() > 0) {
            model.addAttribute("error", "Wrong payment order. Select account and check your balance.");
        } else {
            model.addAttribute("error", "");
        }

        Client user = (Client) request.getSession().getAttribute("user");
        if (user == null || !user.getUsername().equals(user.getUsername()) || !user.getPassword().equals(user.getPassword())) {
            return "redirect:/";
        }
        if (null != status) {
            if (status.equals("all"))
                model.addAttribute("payments", paymentService.getMyPayments(paymentService.getAllPayments(), user.getId()));
            else
                model.addAttribute("payments", paymentService.getMyPayments(paymentService.findByStatus(status), user.getId()));
        } else {
            model.addAttribute("payments", paymentService.getMyPayments(paymentService.getAllPayments(), user.getId()));
        }
        model.addAttribute("clients", clientService.getOtherClients(clientService.getAllClients(), user.getId()));
        model.addAttribute("payment", new Payment());
        return "account";
    }

    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    public String payment(@ModelAttribute Payment payment, Model model, HttpServletRequest request) {
        Client user = (Client) request.getSession().getAttribute("user");
        Client recipient;

        if (user == null || !user.getUsername().equals(user.getUsername()) || !user.getPassword().equals(user.getPassword())) {
            return "redirect:/";
        }

        if ((payment.getAmount() > user.getBalance()) || payment.getClientRecipient().getId() == 0) {
            model.addAttribute("payments", paymentService.getMyPayments(paymentService.getAllPayments(), user.getId()));
            model.addAttribute("clients", clientService.getOtherClients(clientService.getAllClients(), user.getId()));
            return "redirect:account/?status=all&err=1";
        } else {
            paymentService.createPayment(payment);
            recipient = clientService.getClient(payment.getClientRecipient().getId());
            if (payment.getAmount() < 100 && payment.getId() % 10 != 0) {
                user.setBalance(user.getBalance() - payment.getAmount());
                double newBalace = recipient.getBalance() + payment.getAmount();
                recipient.setBalance(newBalace);
                payment.setStatus("paid");
            } else {
                payment.setStatus("available");
            }
            clientService.editClient(user);
            clientService.editClient(recipient);
            payment.setClientPayer(user);
            paymentService.editPayment(payment);
            model.addAttribute("payments", paymentService.getAllPayments());
            return "payment";
        }
    }

    @RequestMapping(value = "/bank", method = RequestMethod.GET)
    public String bank(Model model, HttpServletRequest request, @RequestParam(value = "status") String status) {
        Client user = (Client) request.getSession().getAttribute("user");
        if (user == null || !user.getUsername().equals("bank")) {
            return "redirect:/";
        }

        if (null != status) {
            if (status.equals("all")) model.addAttribute("payments", paymentService.getAllPayments());
            else model.addAttribute("payments", paymentService.findByStatus(status));
        } else {
            model.addAttribute("payments", paymentService.getAllPayments());
        }
        return "bank";
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public String detail(Model model, @RequestParam(value = "id") Long id, HttpServletRequest request) {
        Client user = (Client) request.getSession().getAttribute("user");
        if (user == null || !user.getUsername().equals("bank")) {
            return "redirect:/";
        }

        model.addAttribute("payment", paymentService.getPayment(id));
        return "detail";
    }

    @RequestMapping(value = "/execute", method = RequestMethod.POST)
    public String execute(@ModelAttribute Payment payment, HttpServletRequest request) {
        Client user = (Client) request.getSession().getAttribute("user");
        if (user == null || !user.getUsername().equals("bank")) {
            return "redirect:/";
        }

        Payment paymentFromDb = paymentService.getPayment(payment.getId());
        if (paymentFromDb == null) {
            return "redirect:/detail?id=" + payment.getId();
        }
        Client payer = paymentFromDb.getClientPayer();
        Client recipient = paymentFromDb.getClientRecipient();
        if (payment.getAmount() <= payer.getBalance()) {
            payer.setBalance(payer.getBalance() - paymentFromDb.getAmount());
            recipient.setBalance(recipient.getBalance() + paymentFromDb.getAmount());
            paymentFromDb.setStatus("paid");
        } else {
            paymentFromDb.setStatus("cancelled");
        }
        paymentService.editPayment(paymentFromDb);
        clientService.editClient(payer);
        clientService.editClient(recipient);

        return "redirect:/detail?id=" + payment.getId();
    }

    @RequestMapping(value = "/reject", method = RequestMethod.POST)
    public String reject(@ModelAttribute(value = "payment") Payment payment, HttpServletRequest request) {
        Client user = (Client) request.getSession().getAttribute("user");
        if (user == null || !user.getUsername().equals("bank")) {
            return "redirect:/";
        }

        Payment paymentFromDb = paymentService.getPayment(payment.getId());
        if (paymentFromDb == null) {
            return "redirect:/detail?id=" + payment.getId();
        }
        paymentFromDb.setStatus("cancelled");
        paymentService.editPayment(paymentFromDb);
        return "redirect:/detail?id=" + paymentFromDb.getId();
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request) {
        clear(request);
        return "redirect:/";

    }

    private static void clear(HttpServletRequest request) {
        HttpSession hs = request.getSession();
        Enumeration e = hs.getAttributeNames();
        while (e.hasMoreElements()) {
            String attr = (String) e.nextElement();
            hs.setAttribute(attr, null);
        }
        removeCookies(request);
        hs.invalidate();
    }

    private static void removeCookies(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);
            }
        }
    }

}