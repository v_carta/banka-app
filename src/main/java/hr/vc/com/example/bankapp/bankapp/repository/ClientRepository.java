package hr.vc.com.example.bankapp.bankapp.repository;

import hr.vc.com.example.bankapp.bankapp.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByUsername(String username);
    Client findByAccount(Long account);
}