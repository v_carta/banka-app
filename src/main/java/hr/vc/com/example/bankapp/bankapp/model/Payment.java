package hr.vc.com.example.bankapp.bankapp.model;

import javax.persistence.*;

@Entity
public class Payment {
    @Id
    @GeneratedValue
    private Long id;
    private String status = "available";
    private  Long timestamp = System.currentTimeMillis();
    private double amount;
    @ManyToOne
    private Client clientPayer;
    @ManyToOne
    private Client clientRecipient;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Client getClientPayer() {
        return clientPayer;
    }

    public void setClientPayer(Client clientPayer) {
        this.clientPayer = clientPayer;
    }

    public Client getClientRecipient() {
        return clientRecipient;
    }

    public void setClientRecipient(Client clientRecipient) {
        this.clientRecipient = clientRecipient;
    }
}