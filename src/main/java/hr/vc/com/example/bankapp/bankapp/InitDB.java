package hr.vc.com.example.bankapp.bankapp;

import hr.vc.com.example.bankapp.bankapp.service.ClientService;
import hr.vc.com.example.bankapp.bankapp.service.PaymentService;
import hr.vc.com.example.bankapp.bankapp.model.Client;
import hr.vc.com.example.bankapp.bankapp.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InitDB {
    @Autowired
    private ClientService clientService;
    @Autowired
    private PaymentService paymentService;
    Payment payment1 = new Payment();
    Payment payment2 = new Payment();
    Payment payment3 = new Payment();

    Client client1 = new Client();
    Client client2 = new Client();
    Client client3 = new Client();
    Client bank = new Client();

    @Autowired
    public InitDB(ClientService clientService, PaymentService paymentService) {
        this.clientService = clientService;
        this.paymentService = paymentService;
        addClients();
    }

    public void addClients(){
        client1.setUsername("ivan");
        client1.setPassword("pass1");
        clientService.createClient(client1);

        client2.setUsername("ana");
        client2.setPassword("pass2");
        clientService.createClient(client2);

        client3.setUsername("marko");
        client3.setPassword("pass3");
        clientService.createClient(client3);

        bank.setUsername("bank");
        bank.setPassword("bank");
        clientService.createClient(bank);

        payment1.setAmount(150);
        payment1.setStatus("available");
        payment1.setClientPayer(client1);
        payment1.setClientRecipient(client2);
        paymentService.createPayment(payment1);

        payment2.setAmount(200);
        payment2.setStatus("available");
        payment2.setClientPayer(client2);
        payment2.setClientRecipient(client3);
        paymentService.createPayment(payment2);

        payment3.setAmount(120.7);
        payment3.setStatus("available");
        payment3.setClientPayer(client3);
        payment3.setClientRecipient(client1);
        paymentService.createPayment(payment3);

    }
}
