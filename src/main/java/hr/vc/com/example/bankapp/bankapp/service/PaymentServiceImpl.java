package hr.vc.com.example.bankapp.bankapp.service;

import hr.vc.com.example.bankapp.bankapp.model.Payment;
import hr.vc.com.example.bankapp.bankapp.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public Payment createPayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    @Override
    public Payment getPayment(Long id) {
        return paymentRepository.findOne(id);
    }

    @Override
    public Payment editPayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    @Override
    public List<Payment> findByStatus(String status) {
        return paymentRepository.findByStatus(status);
    }

    @Override
    public List<Payment> getMyPayments(List<Payment> allPayments, Long id) {
        List<Payment> myPayments = new LinkedList<>();
        for (Payment allPayment : allPayments) {
            if (allPayment.getClientPayer().getId() == id) {
                myPayments.add(allPayment);
            }
        }
        return myPayments;
    }
}
